package montero.Q.Gianina.bl.entities;

public class Fecha {
    private String mes; //* usaremos el nombre del mes.Ejemplo: Enero
    private int annio;

    public Fecha() {
    }

    public Fecha(String mes, int annio) {
        this.mes = mes;
        this.annio = annio;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public int getAnnio() {
        return annio;
    }

    public void setAnnio(int annio) {
        this.annio = annio;
    }

    public String toString() {
        return ", Fecha: " + mes + ", " + annio;
    }
}
