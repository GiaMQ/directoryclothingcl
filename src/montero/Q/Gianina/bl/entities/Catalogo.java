package montero.Q.Gianina.bl.entities;

import java.util.ArrayList;

public class Catalogo {
    private int codigoCatalogo;
    private String nombreCatalogo;
    private Fecha fecha;
    private ArrayList<Camiseta> listaCamisetas; //relacion de composicion

    public Catalogo() {
    }

    public Catalogo(int codigoCatalogo, String nombreCatalogo, String mes,int annio) {
        this.codigoCatalogo = codigoCatalogo;
        this.nombreCatalogo = nombreCatalogo;
        this.fecha = new Fecha (mes,annio);
        this.listaCamisetas = new ArrayList<>();
    }

    public int getCodigoCatalogo() {
        return codigoCatalogo;
    }

    public void setCodigoCatalogo(int codigoCatalogo) {
        this.codigoCatalogo = codigoCatalogo;
    }

    public String getNombreCatalogo() {
        return nombreCatalogo;
    }

    public void setNombreCatalogo(String nombreCatalogo) {
        this.nombreCatalogo = nombreCatalogo;
    }

    public Fecha getFecha() {
        return fecha;
    }

    public void setFecha(Fecha fecha) {
        this.fecha = fecha;
    }

    public void crearFecha(String mes,int annio){
        this.fecha = new Fecha (mes,annio);
    }

    public void agregarCamisetas (int id, char tamanno, String descripcion,String imagen, double precio,Color color){
        Camiseta camiseta = new Camiseta(id,tamanno,descripcion,imagen,precio,color);
        listaCamisetas.add(camiseta);
    }

    public String toString() {
        String mensaje = "Catalogo: " + nombreCatalogo +" "+ codigoCatalogo +" "+ fecha +"\n";
        mensaje += " ----------- Lista de Camisetas ---------------\n";

        for(Camiseta camiseta:listaCamisetas){
            mensaje += camiseta.toString() + "\n";
        }
        return mensaje;
    }
}
