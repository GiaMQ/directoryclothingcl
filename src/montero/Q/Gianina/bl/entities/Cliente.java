package montero.Q.Gianina.bl.entities;

import java.util.Objects;

public class Cliente {
    //1.Atributos
    private int codigo;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String direccion;
    private String correo;

    //2.Constructores


    public Cliente() {
    }

    public Cliente(int codigo, String nombre, String primerApellido, String segundoApellido, String direccion, String correo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.direccion = direccion;
        this.correo = correo;
    }

//3. Metodos get() y set()


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    //4.Metodo toString()

    public String toString() {
        return  codigo + "," + nombre + "," + primerApellido + "," + segundoApellido +
                "," + direccion + "," + correo ;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null ) return false;
        if (!(o instanceof Cliente)) return false;
        Cliente cliente = (Cliente) o;

        if (this.getCodigo() == cliente.getCodigo()){
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Objects.hash(codigo);
    }
}
