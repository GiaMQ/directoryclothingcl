package montero.Q.Gianina.bl.entities;

import java.util.Objects;

public class Camiseta {
    //1.Atributos
    private int id;
    private char tamanno;
    private String descripcion;
    private String imagen;
    private double precio;
    private Color color;

    public Camiseta() {
    }

    public Camiseta(int id, char tamanno, String descripcion,String imagen, double precio,Color color) {
        this.id = id;
        this.tamanno = tamanno;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.precio = precio;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public char getTamanno() {
        return tamanno;
    }

    public void setTamanno(char tamanno) {
        this.tamanno = tamanno;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String toString() {
        return  id + "," + tamanno + "," + descripcion + ","+ imagen +"," + precio + "," + color;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null ) return false;
        if (!(o instanceof Camiseta)) return false;
        Camiseta camiseta = (Camiseta) o;

        if(this.getId() == camiseta.getId()){
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Objects.hash(id);
    }
}
