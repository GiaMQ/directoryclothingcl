package montero.Q.Gianina.bl.entities;

import java.util.Objects;

public class Color {
    private int codigo;
    private String descripcion;

    public Color() {
    }

    public Color(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    public String toString() {
        return    codigo + "," + descripcion;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null ) return false;
        if (!(o instanceof Color)) return false;
        Color color = (Color) o;

        if(this.getCodigo() == color.getCodigo()){
            return true;
        }
        return false;
    }


    public int hashCode() {
        return Objects.hash(codigo);
    }
}


