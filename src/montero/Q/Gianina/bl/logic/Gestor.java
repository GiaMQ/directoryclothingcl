package montero.Q.Gianina.bl.logic;

import montero.Q.Gianina.bl.entities.Camiseta;
import montero.Q.Gianina.bl.entities.Catalogo;
import montero.Q.Gianina.bl.entities.Cliente;
import montero.Q.Gianina.bl.entities.Color;
import montero.Q.Gianina.dl.Data;

import java.util.ArrayList;

public class Gestor {

    private Data datos;

    public Gestor(){
        datos = new Data();
    }

    public boolean registarCliente(int codigo, String nombre, String primerApellido, String segundoApellido, String direccion, String correo) {
        Cliente cliente = new Cliente(codigo, nombre, primerApellido, segundoApellido, direccion, correo);
        if(!datos.existeRegistro(cliente)) {
            datos.registrarClientes(cliente);
            return true;
        }
        return false;
    }

    public ArrayList<Cliente> listarClientes(){
        return datos.listaClientes();
    }

    public ArrayList<Color> listarColores() {
        return datos.listaColores();
    }

    public String descripcionColor(int codigoColor) {
        Color descripcionColor = new Color();
        descripcionColor.setCodigo(codigoColor);
        Color obtenerColor = datos.buscarDescripcion(descripcionColor);
        if (obtenerColor != null) {
            return obtenerColor.getDescripcion();
        }
        return null;
    }

    public boolean registarCamisetas ( int id, char tamanno, String descripcion, String imagen,double precio,Color color){
        Camiseta camiseta = new Camiseta(id,tamanno,descripcion,imagen,precio,color);
        if(!datos.existeCamiseta(camiseta)) {
            datos.registrarCamisetas(camiseta);
            return true;
        }
        return false;
    }

    public ArrayList<Camiseta> listarCamisetas(){
        return datos.listaCamisetas();
    }

    public void registarCatalogos (Catalogo catalogo){
        datos.registrarCatalogos(catalogo);
    }

    public ArrayList<Catalogo> listarCatalogo() {
        return datos.listaCatalogo();
    }

    public Camiseta existeCamisetas(int codigo) {
        Camiseta camisetaAbuscar = new Camiseta();
        camisetaAbuscar.setId(codigo);
        return datos.buscarCamiseta(camisetaAbuscar);
    }

    public void agregarCamiseta(Catalogo catalogo,int id, char tamanno, String descripcion,String imagen, double precio,Color color){
        datos.asignarCamisetas(catalogo,id,tamanno,descripcion,imagen,precio,color);
    }


}


