package montero.Q.Gianina.dl;

import montero.Q.Gianina.bl.entities.*;

import java.io.*;
import java.util.ArrayList;

public class Data {

    static final String NOMBRE_ARCHIVO1 = "Clientes.txt";
    static final String NOMBRE_ARCHIVO2 = "Camisetas.txt";
    static final String NOMBRE_ARCHIVO3 = "Colores.txt";
    static final String NOMBRE_ARCHIVO4 = "Catalogo.txt";
    static final char[] tallas = {'L','M','S'};


    private ArrayList<Catalogo> listaCatalogo;
    private ArrayList<Color> listaColor;

    public Data() {
        listaCatalogo = new ArrayList<>();
    }

    public void registrarClientes(Cliente cliente) {
        try {
            FileWriter writer = new FileWriter(NOMBRE_ARCHIVO1, true);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write(cliente.toString());
            buffer.newLine();
            buffer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean existeRegistro(Cliente cliente){
        for (Cliente clienteInfo: listaClientes()){
            if(clienteInfo.equals(cliente))
                return true;
        }
        return false;
    }

    public ArrayList<Cliente> listaClientes() {
        ArrayList<Cliente> listaCliente = new ArrayList<>();
        try {
            FileReader reader = new FileReader(NOMBRE_ARCHIVO1);
            BufferedReader buffer = new BufferedReader(reader);
            String linea;
            while ( (linea = buffer.readLine()) != null) {
                String[] datoCliente = linea.split(",");
                Cliente clientes = new Cliente (Integer.parseInt(datoCliente[0]),datoCliente[1],datoCliente[2],datoCliente[3],datoCliente[4],datoCliente[5]);
                listaCliente.add(clientes);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listaCliente;
    }

    public ArrayList<Color> listaColores() {
        ArrayList<Color> listaColor = new ArrayList<>();
        try {
            FileReader reader = new FileReader(NOMBRE_ARCHIVO3);
            BufferedReader buffer = new BufferedReader(reader);
            String linea;
            while ( (linea = buffer.readLine()) != null) {
               String[] datoColor = linea.split(",");
               Color colores = new Color (Integer.parseInt(datoColor[0]),datoColor[1]);
               listaColor.add(colores);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listaColor;
    }

    public Color buscarDescripcion(Color colorDescripcion){
        listaColor = listaColores();
        for (Color colorTemp:listaColor){
            if(colorTemp.equals(colorDescripcion))
                return  colorTemp;
        }
    return null;
    }

    public void registrarCamisetas(Camiseta camiseta) {
        try {
            FileWriter writer = new FileWriter(NOMBRE_ARCHIVO2, true);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write(camiseta.toString());
            buffer.newLine();
            buffer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean existeCamiseta(Camiseta camiseta){
        for(Camiseta camisetaInfo: listaCamisetas()){
            if(camisetaInfo.equals(camiseta))
                return true;
        }
        return false;
    }

    public ArrayList<Camiseta> listaCamisetas() {
        ArrayList<Camiseta> lista = new ArrayList<>();
        try {
            FileReader reader = new FileReader(NOMBRE_ARCHIVO2);
            BufferedReader buffer = new BufferedReader(reader);
            String linea;
            while ( (linea = buffer.readLine()) != null) {
                String[] datoCamiseta = linea.split(",");
                Color colorCamiseta = new Color(Integer.parseInt(datoCamiseta[5]),datoCamiseta[6]);
                Camiseta camiseta = new Camiseta (Integer.parseInt(datoCamiseta[0]),datoCamiseta[1].charAt(0),
                datoCamiseta[2],datoCamiseta[3],Double.parseDouble(datoCamiseta[4]),colorCamiseta);
                lista.add(camiseta);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lista;
    }

    public void registrarCatalogos(Catalogo catalogo) {
        listaCatalogo.add(catalogo);
    }

    public ArrayList<Catalogo> listaCatalogo(){
        return (ArrayList<Catalogo>) listaCatalogo.clone();
    }

    public Camiseta buscarCamiseta(Camiseta camiseta){
        for (Camiseta camisetaTemp:listaCamisetas()){
            if(camisetaTemp.equals(camiseta))
                return  camisetaTemp;
        }
        camiseta.setId(000);
        return camiseta;
    }

    public void asignarCamisetas(Catalogo catalogo,int id, char tamanno, String descripcion,String imagen, double precio,Color color){
        Catalogo asignarCatalogo = catalogo;
        asignarCatalogo.agregarCamisetas(id,tamanno,descripcion,imagen,precio,color);
    }

}
